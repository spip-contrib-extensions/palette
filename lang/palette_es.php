<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/palette?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'config_desc' => '<strong>Paleta</strong> permite añadir automáticamente un selector de colores a un campo de entrada, simplemente dándole la clase CSS «palette»',
	'config_exemple' => 'Ejemplos para llamar la Paleta',
	'config_exemple_code_source' => 'Ejemplo de código fuente',
	'config_exemple_couleur' => 'color normal',
	'config_exemple_couleur_alpha' => 'Color con transparencia',
	'config_exemples_couleurs_vides' => 'Sin valor predeterminado en el campo',
	'config_lien_doc' => '<a href="http://contrib.spip.net/Palette" class="spip_out">Ver la documentación</a>',
	'config_titre' => 'Configure el selector de color de la paleta.',

	// F
	'fermer' => 'Cerrar la paleta',

	// P
	'palette' => 'Paleta',
	'palette_ecrire' => 'Activar Paleta en el espacio privado',
	'palette_public' => 'Activar Paleta en el sitio público'
);
