<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/palette?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'config_desc' => '<strong>Palette</strong> automatically adds a colour picker wheel to an input field,
  by simply applying the CSS class "palette" to it',
	'config_exemple' => 'Palette call examples:',
	'config_exemple_code_source' => 'Examples code source',
	'config_exemple_couleur' => 'Normal color',
	'config_exemple_couleur_alpha' => 'Color with transparency',
	'config_exemples_couleurs_vides' => 'No default value on the field',
	'config_lien_doc' => '<a href="https://contrib.spip.net/Palette" class="spip_out">Cf. documentation</a>',
	'config_titre' => 'Configure the Palette color picker.',

	// F
	'fermer' => 'Close palette',

	// P
	'palette' => 'Palette',
	'palette_ecrire' => 'Activate Palette for the private area',
	'palette_public' => 'Activate Palette for the public area'
);
